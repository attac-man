/* oglconsole -- gpl license here */
/* Added macro for con_printf() -- lon */
#define OGLCONSOLE_USE_SDL

#ifndef _OGLCONSOLE_H
#define _OGLCONSOLE_H

#ifdef __cplusplus
extern "C" {
#endif

/* Opaque to you you lowly user */
typedef struct _OGLCONSOLE_Console *OGLCONSOLE_Console;

/* Initialize/uninitialize OGLConsole */
OGLCONSOLE_Console OGLCONSOLE_Create(void);
void OGLCONSOLE_InitText(void *console, int res_x, int res_y);
void OGLCONSOLE_Destroy(OGLCONSOLE_Console console);
void OGLCONSOLE_Quit(void);

int command_split(char *cmd, int *argc, char **argv, int max);

/* Set console which has PROGRAMMER focus (not application focus) */

/* This function renders the console */
void OGLCONSOLE_Draw(void);
void OGLCONSOLE_Render(OGLCONSOLE_Console console);

/* Print to the console */
void OGLCONSOLE_Print(char *s, ...);
void OGLCONSOLE_Output(OGLCONSOLE_Console console, const char *s, ...);

#define con_printf(fmt, args...) \
do { \
	printf(fmt, ##args); \
	OGLCONSOLE_Print(fmt, ##args); \
} while(0)

/* Register a callback with the console */
void OGLCONSOLE_EnterKey(void(*cbfun)(OGLCONSOLE_Console console,
			 int argc, char **argv));

/* This function tries to handle the incoming SDL event. In the future there may
 * be non-SDL analogs for input systems such as GLUT. Returns true if the event
 * was handled by the console. If console is hidden, no events are handled. */
#if defined(OGLCONSOLE_USE_SDL)
#include "SDL.h"
int OGLCONSOLE_SDLEvent(SDL_Event * e);
#endif

/* Sets the current console for receiving user input */
void OGLCONSOLE_FocusConsole(OGLCONSOLE_Console console);

/* Sets the current console for making options changes to */
void OGLCONSOLE_EditConsole(OGLCONSOLE_Console console);

/* Sets the dimensions of the console in lines and columns of characters. */
void OGLCONSOLE_SetDimensions(int width, int height);

/* Show or hide the console. */
void OGLCONSOLE_SetVisibility(int visible);

#ifdef __cplusplus
}
#endif

#endif

