/*
  Pacman Arena
  Copyright (C) 2003 Nuno Subtil

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/* $Id: level-demo.h,v 1.1 2003/11/22 16:22:08 nsubtil Exp $ */

#ifndef _LEVEL_DEMO_H
#define _LEVEL_DEMO_H

#define LEVEL_DEMO_WIDTH 30
#define LEVEL_DEMO_HEIGHT 31

const char *level_demo[] = {
	"L------------R  L------------R",
	"|*...........|  |...........*|",
	"|.L--R.L---R.|  |.L---R.L--R.|",
	"|.|  |.|   |.|  |.|   |.|  |.|",
	"|.l--r.l---r.l--r.l---r.l--r.|",
	"|............................|",
	"|.L--R.LR.L--------R.LR.L--R.|",
	"|.|  |.||.l--R  L--r.||.|  |.|",
	"|.l--r.||....|  |....||.l--r.|",
	"|......|l--R.|  |.L--r|......|",
	"l----R.|L--r.l--r.l--R|.L----r",
	"     |.||............||.|     ",
	"     |.||.L--RUL---R.||.|     ",
	"-----r.lr.|L-rUl--R|.lr.l-----",
	"0.........||G UGG ||.........1",
	"-----R.LR.|l------r|.LR.L-----",
	"     |.||.l--------r.||.|     ",
	"     |.||............||.|     ",
	"L----r.||.L--------R.||.l----R",
	"|*.....lr.l---RL---r.lr.....*|",
	"|.L--R........||........L--R.|",
	"|.|  |.L----R.||.L----R.|  |.|",
	"|.l-R|.l----r.lr.l----r.|L-r.|",
	"|...||..................||...|",
	"l-R.||.LR.L--------R.LR.||.L-r",
	"L-r.lr.||.l--R L---r.||.lr.l-R",
	"|......||....| |.....||......|",
	"|.L----rl--R.| |.L---rl----R.|",
	"|.l--------r.l-r.l---------r.|",
	"|*..........................*|",
	"l----------------------------r"
};

#endif

