#ifndef SCORE_H
#define SCORE_H

typedef int score_lock_t;

void rewrite_score_file(void);
int high_score(uint64_t score);
void add_high_score(char *name, uint64_t score);
score_lock_t lock_scores(void);
void unlock_scores(score_lock_t);
void high_scores_init(void);

void debug_print_high_scores(void);

#endif
