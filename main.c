/*
  Pacman Arena
  Copyright (C) 2003 Nuno Subtil

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

static const char cvsid[] = 
  "$Id: main.c,v 1.60 2003/11/27 22:11:57 nsubtil Exp $";

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/gl.h>
#include <SDL.h>
#include <SDL_net.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h> /* basename() */

#include "audio.h"
#include "object.h"
#include "game.h"
#include "map.h"
#include "player.h"
#include "render.h"
#include "m_math.h"
#include "menu.h"
#include "screen.h"
#include "net.h"
#include "oglconsole.h"
#include "score.h"

extern struct game* current_game;

/*
 * TODO: Rename function.  Need to look around code where it calls
 * SDL_Quit() and exit and see if we can call this instead in 
 * those places.
 */
void die(int ret)
{
	OGLCONSOLE_Quit();
	SDL_Quit();
	exit(ret);
}


int parse_resolution(char *res, int *w, int *h, int *bpp)
{
	char res_copy[80];
	char *p;

	snprintf(res_copy, sizeof(res_copy)-1, "%s", res);
	p = strtok(res_copy, "x");
	if (!p)
		goto out_err;
	*w = atoi(p);
	if (*w <= 0)
		goto out_err;

	p = strtok(NULL, "x");
	if (!p)
		goto out_err;
	*h = atoi(p);
	if (*h <= 0)
		goto out_err;

	p = strtok(NULL, "x");
	if (!p)
		goto out_ok;
	*bpp = atoi(p);
	if (*bpp <= 0)
		goto out_err;

out_ok:
	return 0;
out_err:
	printf("Could not parse resolution '%s'\n", res);
	return 1;
}


/* TODO: rewrite parsing routines here */
/* TODO: Add 'map' command to change maps */
static void console_input(OGLCONSOLE_Console console, int argc, char **argv)
{
	char *cmd = argv[0];
	int width, height, x;

	if (argc < 1)
		return;

	if (!strcasecmp(cmd, "quit") ||
	    !strcasecmp(cmd, "exit")) {
		die(0);
	}

	if (!strcasecmp(cmd, "res")) {
		if (argc < 2) {
			con_printf("usage: res <resolution> e.g. 640x480\n");
			return;
		}
		if (parse_resolution(argv[1], &width, &height, &x) != 0) {
			con_printf("usage: res <resolution> e.g. 640x480\n");
			return;
		}
		screen_set_resolution(width, height);
		screen_switch_resolution();
		OGLCONSOLE_InitText(console, width, height);
		return;
	}

	if (!strcasecmp(cmd, "high-scores")) {
		debug_print_high_scores();
		return;
	}

	if (!strncasecmp(cmd, "score", 5)) {
		if (!current_game) {
			con_printf("Start playing, first...\n");
			return;
		}
		if (!current_game->players) {
			con_printf("But, there are no players?\n");
			return;
		}
		for (x = 0; x < current_game->n_players; x++) {
			con_printf("Player %d: %d points, %d lives\n", x+1,
				   current_game->players[x].score,
				   current_game->players[x].lives);
			return;
		}
	}
    
	OGLCONSOLE_Output(console, "Invalid command: %s\n", cmd);
}

void usage(char *prog)
{
	printf("Usage: %s [options]\n"
	       "\t-f\tFullscreen\n"
	       "\t-w\tWindowed\n"
	       "\t-q\tQuiet (disable sound)\n"
	       "\t-r\tResolution <w>x<h> e.g. 640x480\n"
	       "\t-h\tThis help text\n", basename(prog));
}

int main(int argc, char **argv)
{
	int opt, ret = 0;
	int width = 800, height = 600, bpp = 32, fullscreen = 0;
	int enable_sound = 1;

	while ((opt = getopt(argc, argv, "fwr:q")) != EOF) {
		switch(opt) {
		case 'f':
			fullscreen = 1;
			break;
		case 'w':
			fullscreen = 0;
			break;
		case 'q':
			enable_sound = 0;
			break;
		case 'r':
			if (parse_resolution(optarg, &width, &height,
					     &bpp) != 0)
				return 1;
			printf("Set resolution to %dx%dx%d\n",
			       width, height, bpp);
			break;
		default:
			fprintf(stderr, "Invalid command line option: %c\n",
				opt);
			ret = 1;
		case 'h':
			usage(argv[0]);
			return 0;
		}
	}

/*
	render_reshape_window(screen->w, screen->h);
*/

	srand(SDL_GetTicks());

	screen_init(fullscreen, width, height, bpp);

	audio_init(enable_sound);

	/* preload */
/*
	object_read_file("gfx/pacman-moving.3d", &last_update);
	object_read_file("gfx/pacman-dying.3d", &last_update);
	object_read_file("gfx/pacman-stopped.3d", &last_update);
	object_read_file("gfx/ghost-green-moving.3d", &last_update);
	object_read_file("gfx/ghost-green-dying.3d", &last_update);
	object_read_file("gfx/ghost-green-returning.3d", &last_update);

	if(argc > 1 && strcmp(argv[1], "--server") == 0)
		net_server_init();

	if(argc > 1 && strcmp(argv[1], "--connect") == 0)
		net_client_init(argv[2]);
*/

	OGLCONSOLE_Create();
	OGLCONSOLE_EnterKey(console_input);

	high_scores_init();

	for(;;)
		menu_run();

	return 0;
}
