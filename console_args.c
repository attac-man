/**
 * License - GPL
 *
 * Command-line splitter function.
 * Lon Hohberger <lon@metamorphism.com>
 */
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <malloc.h>

typedef enum _command_state {
	STATE_QUOTE = 0x1,
	STATE_LITERAL = 0x2,
	STATE_COPY = 0x4
} command_state_t;


/**
 * Break up a command string into argv values.  Rudimentary.
 * Destroys the value of cmdline (inserts 0s in order to keep argv
 * pointers 0-terminated in typical C-style)
 */
int
command_split(char *cmdline, int *argc, char **argv, int max)
{
	char *cmd_copy = NULL;
	command_state_t state = 0;
	int offset = 0, copy_offset = 0, ret = 1;

	if (max <= 0 || argv == NULL || argc == NULL) {
		errno = EINVAL;
		return -1;
	}

	cmd_copy = strdup(cmdline);
	if (!cmd_copy)
		return -1;

	memset(cmdline, 0, strlen(cmdline)+1);
	*argc = 0;

	while (cmd_copy[copy_offset]) {
		switch(cmd_copy[copy_offset]) {
		case ' ':
		case '\t':
		case '\r':
		case '\n':
			if (state & (STATE_LITERAL | STATE_QUOTE))
				break;
			if (state & STATE_COPY) {
				state &= ~STATE_COPY;
				cmdline[offset++] = 0;
				continue;
			}
			if (!state) {
				copy_offset++;
				continue;
			}
			break;
		case '\\':
			if (state & STATE_LITERAL)
				break;
			state |= STATE_LITERAL;
			++copy_offset;
			continue;

		case '\"':
			if (state & STATE_LITERAL)
				break;
			if (state & STATE_QUOTE) {
				state &= ~STATE_QUOTE;
			} else {
				state |= STATE_QUOTE;
			}
			++copy_offset;
			continue;
		}

		if (!(state & STATE_COPY)) {
			if (*argc >= max)
				goto out;
			state |= STATE_COPY;
			argv[(*argc)++] = &cmdline[offset];
		}

		state &= ~STATE_LITERAL;

		cmdline[offset] = cmd_copy[copy_offset];
		++offset;
		++copy_offset;
	}
	ret = 0;

out:
	if (cmd_copy)
		free(cmd_copy);
	return ret;
}

