#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <assert.h>
#include <errno.h>

#include "oglconsole.h"
#include "score.h"

/* Data stored to disk */
struct high_score {
	char playername[32];
	uint64_t score;
}__attribute__((packed));
#define SCORE_FILE (DATADIR "/high-scores")

#define NR_HIGH_SCORES		10
#define HIGH_SCORES_SIZE	(sizeof(struct high_score) * NR_HIGH_SCORES)
#define SCORE_VALID(sc)		(sc.playername[0] != '\0')

/* .bss initialized on program startup */
static struct high_score *high_scores;

#define TEMPLATE DATADIR "/scores.XXXXXX"

void
rewrite_score_file(void)
{
	int fd, ret, i;
	char *template;
	int score_len = sizeof(struct high_score);
	struct high_score *cur;
	mode_t oldmask;

	template = strdup(TEMPLATE);
	assert(template);

	/* Write out the new scores to a tmp file */
	oldmask = umask(0);
	fd = mkstemp(template);
	if (fd < 0) {
		con_printf("Unable to write out new scores file.\n");
		con_printf("mkstemp failed with %d\n", errno);
		goto out;
	}
	ret = fchmod(fd, 0660);
	if (ret < 0)
		con_printf("fchmod failed with %d\n", errno);

	for (i = 0; i < NR_HIGH_SCORES; i++) {
		if (!SCORE_VALID(high_scores[i]))
			break;
		ret = write(fd, &high_scores[i], score_len);
		if (ret != score_len) {
			if (ret < 0 && errno == EINTR) {
				i--;
				continue;
			}
			cur = &high_scores[i];
			close(fd);
			unlink(template);
			goto out;
		}
	}
	close(fd);

	/* Move the tmp file into place */
	ret = rename(template, SCORE_FILE);
	if (ret != 0)
		con_printf("failed to move new scores file into place [%d]\n",
			   errno);
out:
	free(template);
	umask(oldmask);
	return;
}

/*
 * The high scores list is pretty small, so it's just an array.  Insertion
 * is done by allocating a new array, and copying the old contents into it,
 * inserting the score in its proper place.  The last element falls off the
 * list.
 */
void
insert_score(char *name, uint64_t score, int slot)
{
	struct high_score *new_high_scores = malloc(HIGH_SCORES_SIZE);
	int i, j;

	assert(new_high_scores);

	for (i = 0, j = 0; i < NR_HIGH_SCORES; i++) {
		if (i == slot) {
			new_high_scores[i].score = score;
			strcpy(new_high_scores[i].playername, name);
			continue;
		} else
			new_high_scores[i] = high_scores[j];
		j++;
	}

	free(high_scores);
	high_scores = new_high_scores;
}

void
add_high_score(char *name, uint64_t score)
{
	int i;

	for (i = 0; i < NR_HIGH_SCORES; i++) {
		if (score > high_scores[i].score) {
			insert_score(name, score, i);
			break;
		}
	}
}

int
high_score(uint64_t score)
{
	if (score > high_scores[NR_HIGH_SCORES-1].score)
		return 1;
	return 0;
}

void
flock_init(struct flock *fl, short type)
{
	memset(fl, 0, sizeof(*fl));
	fl->l_type = type;
	fl->l_whence = SEEK_SET;
	fl->l_start = 0;
	fl->l_len = 0;
}

score_lock_t
lock_scores(void)
{
	int fd, ret;
	struct flock fl;

	flock_init(&fl, F_WRLCK);

	fd = open(SCORE_FILE, O_RDWR);
	if (fd < 0) {
		con_printf("failed to open scores file!\n");
		return -1;
	}

retry:
	ret = fcntl(fd, F_SETLKW, &fl);
	if (ret < 0) {
		if (errno == EINTR)
			goto retry;
		con_printf("failed to lock scores file!\n");
		close(fd);
		return -1;
	}

	return fd;
}

void
unlock_scores(score_lock_t lock)
{
	int fd = (int)lock;
	int ret;
	struct flock fl;

	flock_init(&fl, F_UNLCK);

retry:
	ret = fcntl(fd, F_SETLKW, &fl);
	if (ret < 0) {
		if (errno == EINTR)
			goto retry;
		con_printf("Failed to release lock on scores file!\n");
	}

	close(fd);
}

void
debug_print_high_scores(void)
{
	int i;

	for (i = 0; i < NR_HIGH_SCORES; i++) {
		if (!SCORE_VALID(high_scores[i]))
			break;
		con_printf("%s  ->  %llu\n", high_scores[i].playername,
			   high_scores[i].score);
	}
}

void
high_scores_init(void)
{
	int fd, ret;
	struct high_score score;
	int hs_size = sizeof(struct high_score);

	fd = open(SCORE_FILE, O_CREAT);
	if (fd < 0) {
		perror("open");
		printf("Unable to open scores file.\n");
		return;
	}

	/* File is sorted from highest to lowest */
	high_scores = malloc(HIGH_SCORES_SIZE);
	memset(high_scores, 0, HIGH_SCORES_SIZE);
	assert(high_scores);
	while ((ret = read(fd, &score, hs_size)) == hs_size)
		add_high_score(score.playername, score.score);
	close(fd);
}
